<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatingTheJobUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('job_uploads', function (Blueprint $table) {
          $table->increments('id');
          $table->string('file_name');
          $table->integer('application_id')->unsigned();
          $table->timestamps();
          $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
