<?php

use Illuminate\Database\Seeder;

class departmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('departments')->insert([
        'department_name' => 'accounting',
      ]);
      DB::table('departments')->insert([
        'department_name' => 'sales',
      ]);
      DB::table('departments')->insert([
        'department_name' => 'marketing',
      ]);
      DB::table('departments')->insert([
        'department_name' => 'IT',
      ]);
      DB::table('departments')->insert([
        'department_name' => 'administration',
      ]);
      DB::table('departments')->insert([
        'department_name' => 'projects',
      ]);
      DB::table('departments')->insert([
        'department_name' => 'Graphic Designer',
      ]);
    }
}
