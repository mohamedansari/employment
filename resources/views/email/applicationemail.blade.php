<h1> New application </h1>
name: {{$application->name}} <br />
email: {{$application->email}}<br />
phone: {{$application->phone_number}}<br />
department: {{$application->department->department_name}}<br />
<br /><br />
files:
@foreach($application->uploads as $upload)
  <a href="{{\Storage::url($upload->file_name)}}" > file </a><br />
@endforeach
