@extends('includes.master')
@section('css')
<link rel="stylesheet" href="css/dash.css" >
@endsection
@section('content')
<div class="container-fluid">
     <div class="row">
       <div class="col-sm-3 col-md-2 sidebar">
         <ul class="nav nav-sidebar">
           <li class="active"><a href="#">applications <span class="sr-only">(current)</span></a></li>
         </ul>
       </div>
       <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
         <h1 class="page-header">Dashboard</h1>

         <h2 class="sub-header">job application</h2>
         <div class="">
      <div class="row">
      <div class="  toppad  pull-right  ">

      </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Application Profile</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">  </div>
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td> <b>Personal Information: </b></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td> name:</td>
                        <td> {{$application->name}}</td>
                      </tr>
                      <tr>
                        <td>email:</td>
                        <td>{{$application->email}}</td>
                      </tr>
                      <tr>
                        <td>Phone number :</td>
                        <td>{{$application->phone_number}}</td>
                      </tr>
                      <tr>
                        <td>Application department:</td>
                        <td>{{$application->department->department_name}}</td>
                      </tr>
                      <tr>
                        <td>Application files:</td>
                        @php $i=0; @endphp
                        <td>@foreach($application->uploads as $upload)
                          @php $i++ @endphp
                          <a href="{{\Storage::url($upload->file_name)}}" > file {{$i}}</a><br />
                        @endforeach</td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>
       </div>
     </div>
   </div>
   @endsection
