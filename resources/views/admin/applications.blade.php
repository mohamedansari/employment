@extends('includes.master')
@section('css')
<link rel="stylesheet" href="css/dash.css" >
@endsection
@section('content')
<div class="container-fluid">
     <div class="row">
       <div class="col-sm-3 col-md-2 sidebar">
         <ul class="nav nav-sidebar">
           <li class="active"><a href="#">applications <span class="sr-only">(current)</span></a></li>
         </ul>
       </div>
       <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
         <h1 class="page-header">Dashboard</h1>

         <h2 class="sub-header">job application</h2>
         <div class="table-responsive">
           <table class="table table-striped">
             <thead>
               <tr>
                 <th>#</th>
                 <th>name</th>
                 <th>email</th>
                 <th>department</th>
                 <th>number of files</th>
               </tr>
             </thead>
             <tbody>
               @php $i=0; @endphp
               @foreach($applications as $application)
              @php $i++ @endphp
               <tr>

                 <td>{{$i}}</td>
                 <td><a href="/application/{{$application->id}}">{{$application->name}}</a></td>
                 <td>{{$application->email}}</td>
                 <td>{{$application->department->department_name}}</td>
                 <td>{{$application->uploads->count()}}</td>
               </tr>
               @endforeach

             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
   @endsection
