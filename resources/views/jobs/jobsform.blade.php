@extends('includes.master')
@section('content')
@include('includes.errors')
<div class="container">
  <div class="col-md-5">
    <form class="form-horizontal" action='{{route("apply")}}' method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <fieldset>
        <div id="legend">
          <legend class="">Job Application Form</legend>
        </div>
        <div class="control-group">
          <!-- Username -->
          <label class="control-label"  for="name">name</label>
          <div class="controls">
            <input type="text" id="name" name="name" placeholder="Full name" class="form-control">
            <p class="help-block">Please enter your full name</p>
          </div>
        </div>

        <div class="control-group">
          <!-- E-mail -->
          <label class="control-label" for="email">E-mail</label>
          <div class="controls">
            <input type="text" id="email" name="email" placeholder="" class="form-control">
            <p class="help-block">Please provide your E-mail</p>
          </div>
        </div>

        <div class="control-group">
          <!-- Phone number-->
          <label class="control-label" for="phone">Phone Number</label>
          <div class="controls">
            <input type="text" id="Phone" name="Phone" placeholder="Phone number" class="form-control">
            <p class="help-block">The phone number must be 8 digits (only numbers)</p>
          </div>
        </div>
        <div class="control-group">
          <!-- Departments-->
          <label class="control-label" for="departments">Department</label>
          <select class="form-control" name="departments">
            @foreach($departments as $department)
              <option value="{{$department->id}}">{{$department->department_name}}</option>
            @endforeach
          </select>
        </div>
        <div class="control-group">
          <!-- File Upload-->
          <label class="control-label" for="file">File Upload</label>
          <div class="controls">
            <input type="file" id="upload" name="uploads[]"   class="form-control">
            <div id="moreupload" >
            </div>
            <button type="button" class="btn btn-success pull-right" id="addmorebtn">add more +</button>

            <p class="help-block">file must be <font color="red"> PDF </font> or <font color="red">Word file (.doc)</font> only </p>
          </div>
        </div>


        <div class="control-group">
          <!-- Submit -->
          <div class="controls">
            <button type="submit" class="btn btn-success">Apply Now </button>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
</div>
@endsection
@section('js')
<script src="js/jobs.js"></script>
@endsection
