@extends('includes.master')
@section('content')
<div class="jumbotron">
      <div class="container">
        <h1>Hello, world!</h1>
        <p>This is a test website to submit job application for different departments</p>
        <p><a class="btn btn-primary btn-lg" href="{{route('jobs')}}" role="button">Apply now </a></p>
      </div>
    </div>
@endsection
