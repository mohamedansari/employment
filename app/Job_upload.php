<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_upload extends Model
{
  protected $fillable = ['file_name','application_id'];
    public function application(){
      return $this->belongsTo('App\Application');
    }
}
