<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Application;
class admin extends Controller
{
    public function index(){
      return View::make('admin.applications',array(
        'applications' => Application::get(),
      ));
    }
    public function applicationdetails($id){
      $application = Application::find($id);
      return View::make('admin.applicationdetails',array(
        'application' => $application,
      ));
    }
}
