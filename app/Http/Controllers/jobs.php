<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Illuminate\Support\Facades\Validator;
use App\Application;
use App\Job_upload;
use App\department;
use App\Mail\applicationEmail;
class jobs extends Controller
{
  public function index(){
    return View::make('jobs/jobsform',array(
      'departments' => department::get(),
    ));
  }
  public function save(Request $data){
    $validator = Validator::make($data->all(), [
      'name' => 'required|max:255',
      'Phone' => 'required|digits:8',
      'email' => 'required|email|max:255|unique:users',
      'departments' => 'required|exists:departments,id',
      'uploads' => 'required|array|min:1',
      'uploads.*' => 'file|mimes:pdf,doc,docx'
      ]);
      if ($validator->fails()) {
           return redirect('/jobs')
                       ->withErrors($validator)
                       ->withInput();
       }
       $application = new Application();
       $application->name =$data->name;
       $application->email = $data->email;
       $application->phone_number = $data->Phone;
       $application->department_id = $data->departments;
       $application->save();
       foreach($data->uploads as $upload){
         $filename = $upload->store('public/uploads');
         Job_upload::create([
            'file_name' => $filename,
            'application_id' => $application->id,
        ]);
       }
       $email = new applicationEmail($application);

       \Mail::to('admin@employment.com')->send($email);
       return View::make('jobs/finished');

  }

}
