<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{

    public function uploads(){
      return $this->hasMany('App\Job_upload');
    }
    public function department(){
      return $this->belongsTo('App\department');
    }
}
